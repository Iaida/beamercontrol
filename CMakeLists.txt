cmake_minimum_required(VERSION 3.6)
project(BeamerControl)


#SET(CMAKE_CXX_FLAGS "-std=c++14 -Wall -Wno-reorder")
SET(CMAKE_CXX_FLAGS "-std=c++14 -Wall -Wno-reorder -O3 -s")


set(Boost_USE_STATIC_LIBS OFF)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME OFF)

find_package(Boost 1.45.0 COMPONENTS filesystem iostreams system)
include_directories(${Boost_INCLUDE_DIRS})

set(SOURCE_FILES
        main.cpp
        Response.h
        Changes.h
        Client.h
        Request.h)
add_executable(BeamerControl ${SOURCE_FILES})

if(Boost_FOUND)
    target_link_libraries(BeamerControl ${Boost_LIBRARIES})
endif()
target_link_libraries(BeamerControl pthread)
target_link_libraries(BeamerControl ncurses)