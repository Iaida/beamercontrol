#pragma once

#include <vector>
#include "Request.h"

// save changes made to revert them
struct Changes
{
    Changes()
            : horizontalChange(0),
              verticalChange(0)
    {}

    short horizontalChange;
    short verticalChange;

    void interpretResponse(const Response & response)
    {
        int value = std::stoi(response.value);
        if(response.command == "LSRH")
            horizontalChange += value;
        else if (response.command == "LSLF")
            horizontalChange -= value;
        else if (response.command == "LSUP")
            verticalChange += value;
        else if (response.command == "LSDW")
            verticalChange -= value;
    }
    // returns pair of messages which restore the values from the start of program
    // messages may not change the value by more than 1000 at once -> send multiple per axis
    std::vector<Request> toResetMessages() const
    {
        std::vector<Request> resetMessages;

        std::string horizontalCommand = (horizontalChange > 0) ? "LSLF" : "LSRH";
        short horizontalToChange = (horizontalChange > 0) ? horizontalChange : -horizontalChange;
        while (horizontalToChange > 0)
        {
            short messageChange;
            if (horizontalToChange > 1000)
            {
                messageChange = 1000;
                horizontalToChange -= 1000;
            }
            else
            {
                messageChange = horizontalToChange;
                horizontalToChange = 0;
            }
            Request request(horizontalCommand, "", std::to_string(messageChange), "");
            resetMessages.emplace_back(request);
        }

        std::string verticalCommand = (verticalChange > 0) ? "LSDW" : "LSUP";
        short verticalToChange = (verticalChange > 0) ? verticalChange : -verticalChange;
        while (verticalToChange > 0)
        {
            short messageChange;
            if (verticalToChange > 1000)
            {
                messageChange = 1000;
                verticalToChange -= 1000;
            }
            else
            {
                messageChange = verticalToChange;
                verticalToChange = 0;
            }
            Request request(verticalCommand, "", std::to_string(messageChange), "");
            resetMessages.emplace_back(request);
        }

        return resetMessages;
    }
};
