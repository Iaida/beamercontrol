
#pragma once

#include <boost/asio.hpp>
#include <iostream>

struct Client
{
    boost::asio::io_service& io_service;
    boost::asio::ip::tcp::socket socket;

    Client(boost::asio::io_service& svc, const std::string & host, const std::string & port)
            : io_service(svc), socket(io_service)
    {
        boost::asio::ip::tcp::resolver resolver(io_service);
        boost::asio::ip::tcp::resolver::iterator endpoint = resolver.resolve(boost::asio::ip::tcp::resolver::query(host, port));
        boost::asio::connect(socket, endpoint);
    };

    void send(std::string const& message)
    {
//        std::cout << "sending " + message;
        socket.send(boost::asio::buffer(message));
    }

    // blocks until message received
    std::string receive()
    {
        boost::asio::streambuf buffer;
        boost::asio::read_until(socket, buffer, "\x0D");

        std::string s((std::istreambuf_iterator<char>(&buffer)),
                      std::istreambuf_iterator<char>());
//        std::cout << "received " + s;
        return s;
    }
};