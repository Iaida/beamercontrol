#pragma once

#include <string>

struct Request
{
    std::string command; // called mnemonic in manual
    std::string modifier;
    std::string values;
    std::string target;

    Request()
    {
    }

    Request(const std::string &command,
            const std::string &modifier,
            const std::string &values,
            const std::string &target)
            : command(command),
              modifier(modifier),
              values(values),
              target(target)
    {}

    std::string toFormattedMessage() const
    {
        return ":" + command + ' ' + modifier + ' ' + values + ' ' + target + "\x0D";
    }

    std::string toString() const
    {
        return "Request: Command: " + command +
               " Modifier: " + modifier +
               " Value: " + values +
               " Target: " + target;
    }
};