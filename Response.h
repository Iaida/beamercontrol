#pragma once

#include <string>

struct Response
{
    std::string address;
    std::string command;
    std::string value;
    std::string error;

    // parse from string, refer to manual for syntax
    // address are 3 bytes starting from index 1
    // command are 4 bytes starting from index 5
    // value are 6 bytes starting from index 10
    Response(const std::string & string)
            : address(string.substr(1, 3)),
              command(string.substr(5, 4)),
              value(string.substr(10, 6))
    {
        // error codes
        if (value[0] == '!')
        {
            switch (value[5])
            {
                case '1':
                    error = "Access denied";
                    break;
                case '2':
                    error = "Not available";
                    break;
                case '3':
                    error = "Not implemented";
                    break;
                case '4':
                    error = "Value out of range";
                default:
                    break;
            }
        }
        else if (value == "e0001")
        {
            // description string follows after space
            std::string description = string.substr(17);
            // remove last character '\x0D'
            description.pop_back();
            value = description;
        }

    }
    Response() = default;

    bool hasError() const
    {
        return !error.empty();
    }

    std::string toString() const
    {
        return "Response: Address: " + address + " Command: " + command + " Value: " + value;
    }
};