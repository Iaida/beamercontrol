// Author: Florian Habib (inf101979)

// based on:
// http://forums.codeguru.com/showthread.php?523737-Detect-arrow-key-press-in-c-for-linux-only-80-of-the-job-is-done&s=b0f093316ecf96cbd0b9003f5fbcbf44&p=2067350#post2067350
// http://stackoverflow.com/questions/27672591/boost-asio-send-and-receive-messages/27672995#27672995

#include "Response.h"
#include "Changes.h"
#include "Client.h"

#include <fstream>
#include <ncurses.h>
#include <iostream>
#include <chrono>
#include <thread>


const std::string BACKUP_COMMANDS_FILENAME = "backup commands.txt";

// port from manual
const std::string PORT = "1025";
const std::map<std::string, std::string> NAME_IP_MAP =
        {
                {"front", "172.16.9.111"},
                {"down",  "172.16.9.112"},
                {"right", "172.16.9.113"},
                {"left",  "172.16.9.114"}
        };

// step for lens shifts, possible steps 0-1000, see manual notes for lens shift messages
std::string STEP = "1000";



// https://stackoverflow.com/questions/6089231/getting-std-ifstream-to-handle-lf-cr-and-crlf/6089413#6089413
std::istream& safeGetline(std::istream& is, std::string& t)
{
    t.clear();

    // The characters in the stream are read one-by-one using a std::streambuf.
    // That is faster than reading them one-by-one using the std::istream.
    // Code that uses streambuf this way must be guarded by a sentry object.
    // The sentry object performs various tasks,
    // such as thread synchronization and updating the stream state.

    std::istream::sentry se(is, true);
    std::streambuf* sb = is.rdbuf();

    for(;;) {
        int c = sb->sbumpc();
        switch (c) {
            case '\n':
                return is;
            case '\r':
                if(sb->sgetc() == '\n')
                    sb->sbumpc();
                return is;
            case std::streambuf::traits_type::eof():
                // Also handle the case when the last line has no line ending
                if(t.empty())
                    is.setstate(std::ios::eofbit);
                return is;
            default:
                t += (char)c;
        }
    }
}


bool saveBackupToFile(Client &client, const std::string &backupCommandsFilename, const std::string &backupFilename)
{
    // contains one line for each relevant command
    std::ifstream backupCommandsFile(backupCommandsFilename, std::ios::in);
    // overrides file if it already existed
    std::ofstream backupFile(backupFilename, std::ios::out | std::ios::trunc);

    if (backupCommandsFile.is_open() && backupFile.is_open())
    {
        // read selected commands from backupCommandsFile
        std::string currentLine;
        while (safeGetline(backupCommandsFile, currentLine))
        {
            // skip empty lines and lines starting with '#'
            if (!(currentLine.empty() || currentLine[0] == '#'))
            {
                // assign command and target depending on existence of a space-separator
                size_t spaceIndex = currentLine.find(' ');
                std::string currentCommand = spaceIndex == std::string::npos ?
                                             currentLine :
                                             currentLine.substr(0, spaceIndex);
                std::string currentTarget = spaceIndex == std::string::npos ?
                                            "" :
                                            currentLine.substr(spaceIndex + 1);

                // create get message
                Request getRequest = Request(currentCommand, "?", "", currentTarget);
                // send get message
                std::cout << getRequest.toString() << '\n';
                client.send(getRequest.toFormattedMessage());

                // wait for response
                Response response = client.receive();
                std::cout << response.toString() << '\n';

                if (response.hasError())
                {
                    std::cout << "error message received for request: " + getRequest.toString() << '\n';
                    std::cout << "Error: " + response.error << '\n';
                    std::cout << "skipping this command \n";
                }
                else
                {
                    // create set message from get-value response
                    Request setRequest(currentCommand, "", response.value, currentTarget);

                    // save set message in text backupFile
                    backupFile << setRequest.toFormattedMessage();
                }
            }
        }
        return true;
    }
    else
        return false;
}

bool loadSettingsFromBackup(Client &client, const std::string & filename)
{
    std::ifstream file(filename, std::ios::in);
    if (file.is_open())
    {
        std::string line;
        while (safeGetline(file, line) && !line.empty())
        {
            // add carriage return back
            client.send(line + "\x0D");
            std::cout << "Formatted Request: " + line << '\n';

            Response response = client.receive();
            std::cout << response.toString() << '\n';

            if (response.hasError())
            {
                std::cout << "Error message received for request: " + line << '\n';
                std::cout << "Error: " + response.error << '\n';
                if (!(response.error == "Access denied"))
                    return false;
                else
                {
                    std::cout << "Access denied is non-critical error, continuing... \n";
                }

            }
        }
        return true;
    }
    else
    {
        std::cout << "error opening file named " << filename << '\n';
        return false;
    }

}




int main(int argc, char *argv[])
{
    if (argc >= 2)
    {
        std::string firstParameter = argv[1];

        // validate first parameter
        auto it = NAME_IP_MAP.find(firstParameter);
        if (it == NAME_IP_MAP.end())
        {
            std::cout << "wrong additional parameter used, use 'front', 'down', 'right' or 'left' \n";
            return EXIT_FAILURE;
        }

        std::string clientIp = it->second;
        boost::asio::io_service svc;
        // create client with set ip and port
        Client client(svc, clientIp, PORT);

        // send 'power on' message to make sure the beamer is on
        client.send(Request("POWR", "", "1", "").toFormattedMessage());
        client.receive();

        if (argc == 2 || argc == 3)
        {
            if (argc == 3)
            {
                STEP = argv[2];
            }

            Changes changes;
            Response response;

            // ncurses library keyboard input, necessary for arrow keys
            initscr();
            crmode();
            keypad(stdscr, true);
            noecho();
            clear();
            mvprintw(0, 0, "Press 'q' to quit with current lens shift,");
            mvprintw(1, 0, "Press 'r' to reset lens shift to program start,");
            mvprintw(2, 0, "Press arrow keys for horizontal and vertical lens shifts");
            refresh();

            int key = getch();

            while(key != ERR)
            {
                mvprintw(4, 0, response.toString().c_str());
                move(3, 0);
                clrtoeol();

                switch(key)
                {
                    case KEY_RIGHT:
                        printw("RIGHT key, Lens Shift Right %s", STEP.c_str());
                        client.send(Request("LSRH", "", STEP, "").toFormattedMessage());
                        response = client.receive();
                        printw("Response: %s", response.toString().c_str());

                        if (response.hasError())
                        {
                            printw("Error received after lens shift: %s", response.error.c_str());
                        }
                        else
                        {
                            changes.interpretResponse(response);
                        }

                        break;

                    case KEY_LEFT:
                        printw("LEFT key, Lens Shift Left %s", STEP.c_str());
                        client.send(Request("LSLF", "", STEP, "").toFormattedMessage());
                        response = client.receive();
                        printw("Response: %s", response.toString().c_str());
                        if (response.hasError())
                        {
                            printw("Error received after lens shift: %s", response.error.c_str());
                        }
                        else
                        {
                            changes.interpretResponse(response);
                        }
                        break;

                    case KEY_UP:
                        printw("UP key, Lens Shift Up %s", STEP.c_str());
                        client.send(Request("LSUP", "", STEP, "").toFormattedMessage());
                        response = client.receive();
                        printw("Response: %s", response.toString().c_str());
                        if (response.hasError())
                        {
                            printw("Error received after lens shift: %s", response.error.c_str());
                        }
                        else
                        {
                            changes.interpretResponse(response);
                        }
                        break;

                    case KEY_DOWN:
                        printw("DOWN key, Lens Shift Down %s", STEP.c_str());
                        client.send(Request("LSDW", "", STEP, "").toFormattedMessage());
                        response = client.receive();
                        printw("Response: %s", response.toString().c_str());
                        if (response.hasError())
                        {
                            printw("Error received after lens shift: %s", response.error.c_str());
                        }
                        else
                        {
                            changes.interpretResponse(response);
                        }
                        break;

                    case 'c':
                        printw("c key, Lens Shift Calibration");
                        // TODO : set-message without notes for parameters in manual ???
                        client.send(Request("LSDCA", "", "", "").toFormattedMessage());
                        response = client.receive();
                        printw("Response: %s", response.toString().c_str());
                        if (response.hasError())
                        {
                            printw("Error received after lens shift: %s", response.error.c_str());
                        }
                        break;

                    case 'r':
                    {
                        printw("r key, Resetting values...");
                        std::vector<Request> resetMessages = changes.toResetMessages();
                        bool result = true;
                        for (const Request & request : resetMessages)
                        {
                            client.send(request.toFormattedMessage());
                            Response resetResponse = client.receive();
                            if (resetResponse.hasError())
                            {
                                result = false;
                                break;
                            }
                            else
                            {
                                // prevents weird errors where messages interfere with one another ???
                                sleep(1);
                            }
                        }
                        printw(result ? "finished" : "failed");
                        // reset change values to 0
                        if (result)
                            changes = Changes();
                        break;
                    }

                    case 'q':
                        endwin();
                        return EXIT_SUCCESS;

                    default:
                        printw("Unmatched, Key was %c", (char)key);
                        break;
                }
                refresh();
                key = getch();
            }
            endwin();
            return EXIT_SUCCESS;
        }

        else if (argc == 4)
        {
            // validate second parameter
            std::string secondParameter = argv[2];
            std::string fileName = argv[3];
            if (secondParameter == "save")
            {
                std::cout << "saving backup file to " + fileName + "... \n";
                bool result = saveBackupToFile(client, BACKUP_COMMANDS_FILENAME, fileName);
                std::cout << (result ? "finished \n" : "failed \n");
                return EXIT_SUCCESS;
            }
            else if (secondParameter == "load")
            {
                std::cout << "loading backup file from " + fileName + "... \n";
                bool result = loadSettingsFromBackup(client, fileName);
                std::cout << (result ? "finished \n" : "failed \n");
                return EXIT_SUCCESS;
            }
            else
            {
                std::cout << "wrong additional parameter used, use 'save' or 'load' \n";
                return EXIT_FAILURE;
            }

        }
        else
        {
            std::cout << "wrong number of additional parameters used \n";
            std::cout << "use only 'front', 'down', 'right' or 'left' as first parameter to adjust lens shift \n";
            std::cout << "use additional parameters 'save' or 'load' followed by a filename to save and restore backup settings \n";
            return EXIT_FAILURE;
        }

    }

    else
    {
        std::cout << "no additional parameters used \n";
        std::cout << "use only 'front', 'down', 'right' or 'left' as first parameter to adjust lens shift \n";
        std::cout << "use additional parameters 'save' or 'load' followed by a filename to save and restore backup settings \n";
        return EXIT_FAILURE;
    }
};
