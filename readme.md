# BeamerControl
 Allows adjustment of a beamers lens shift and ex- and importing of settings via network.  
 Only `front`, `down`, `right` and `left` are valid second parameters, which are mapped to their respective IP addresses.

## Dependencies:
 * boost
 * ncurses

## Lens Shift Usage:
 * start with `./BeamerControl [front,down,right,left]`
 * use arrow-keys to manipulate lens shift
 * use `r` to reset lens shift to starting values
 * use `q` to exit program with current lens shift values

## Managing Backups
 Allows saving of Beamer settings to a textfile, which can be loaded to apply the saved settings
 * saving to a file: `./BeamerControl [front,down,right,left] save savefile.txt`
 * loading from a file: `./BeamerControl [front,down,right,left] load loadfile.txt`

## Backup Commands File
 The file `backup commands.txt` contains a list of relevant commands/settings which are to be saved.  
 Empty lines and lines starting with `#` are ignored by the parser.  
 Lines starting with `#` are either comments or the category of commands in the beamer manual which also serves as submenus when changing settings via the remote control.  




 